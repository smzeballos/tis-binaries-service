package com.umss.tis.binaries.service.controller;

/**
 * @author Santiago Mamani
 */
public final class Constants {

    private Constants() {
    }

    public static class BasePath {

        public static final String SYSTEM = "/system";

        public static final String BINARIES = "/binaries";

        public static final String SYSTEM_BINARIES = SYSTEM + "/binaries";
    }
}
