package com.umss.tis.binaries.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Santiago Mamani
 */
@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BinaryCanNotReadException extends RuntimeException {

    public BinaryCanNotReadException(String message) {
        super(message);
    }
}
