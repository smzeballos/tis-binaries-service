package com.umss.tis.binaries.service.controller.system;

import com.umss.tis.binaries.api.model.Binary;
import com.umss.tis.binaries.service.controller.Constants;
import com.umss.tis.binaries.service.service.BinaryCreateService;
import com.umss.tis.binaries.service.service.BinaryUpdateService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.context.annotation.RequestScope;
import org.springframework.web.multipart.MultipartFile;

/**
 * @author Santiago Mamani
 */
@Api(
        tags = "binary-controller",
        description = "Operations over binaries"
)
@RequestMapping(value = Constants.BasePath.SYSTEM_BINARIES)
@RestController
@RequestScope
public class SystemBinaryController {

    @Autowired
    private BinaryCreateService binaryCreateService;

    @Autowired
    private BinaryUpdateService binaryUpdateService;

    @ApiOperation(
            value = "Upload a binary content"
    )
    @RequestMapping(
            method = RequestMethod.POST,
            consumes = "multipart/form-data"
    )
    public Binary uploadBinary(@RequestPart(value = "multipartFile") MultipartFile multipartFile) {
        binaryCreateService.setFile(multipartFile);
        binaryCreateService.execute();

        return binaryCreateService.getBinary();
    }

    @ApiOperation(
            value = "Update a binary content"
    )
    @RequestMapping(
            value = "/{binaryId}",
            method = RequestMethod.PUT,
            consumes = "multipart/form-data"
    )
    public Binary updateBinary(@PathVariable("binaryId") String binaryId,
                               @RequestPart(value = "multipartFile") MultipartFile multipartFile) {
        binaryUpdateService.setBinaryId(binaryId);
        binaryUpdateService.setFile(multipartFile);
        binaryUpdateService.execute();

        return binaryUpdateService.getBinary();
    }
}
