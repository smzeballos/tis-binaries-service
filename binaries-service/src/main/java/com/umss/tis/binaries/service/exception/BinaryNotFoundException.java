package com.umss.tis.binaries.service.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * @author Santiago Mamani
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class BinaryNotFoundException extends RuntimeException {

    public BinaryNotFoundException(String message) {
        super(message);
    }
}
