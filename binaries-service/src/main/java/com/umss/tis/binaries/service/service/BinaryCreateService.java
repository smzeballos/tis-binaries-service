package com.umss.tis.binaries.service.service;

import com.umss.tis.binaries.service.model.domain.BinaryImpl;
import org.springframework.stereotype.Service;

/**
 * @author Santiago Mamani
 */
@Service
public class BinaryCreateService extends AbstractBinaryService {

    @Override
    protected BinaryImpl loadBinaryInstance() {
        return new BinaryImpl();
    }
}
