package com.umss.tis.binaries.service.model.repository;

import com.umss.tis.binaries.service.model.domain.BinaryImpl;
import org.springframework.data.mongodb.repository.MongoRepository;

/**
 * @author Santiago Mamani
 */
public interface BinaryRepository extends MongoRepository<BinaryImpl, String> {
}
