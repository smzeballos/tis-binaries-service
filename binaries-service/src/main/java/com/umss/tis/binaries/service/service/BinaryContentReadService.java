package com.umss.tis.binaries.service.service;

import com.umss.tis.binaries.service.exception.BinaryNotFoundException;
import com.umss.tis.binaries.service.model.domain.BinaryContent;
import com.umss.tis.binaries.service.model.domain.BinaryImpl;
import com.umss.tis.binaries.service.model.repository.BinaryContentRepository;
import com.umss.tis.binaries.service.model.repository.BinaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Santiago Mamani
 */
@Service
public class BinaryContentReadService {

    private String binaryId;

    private byte[] bytes;

    private BinaryImpl binary;

    @Autowired
    private BinaryContentRepository binaryContentRepository;

    @Autowired
    private BinaryRepository binaryRepository;

    public void execute() {
        binary = findBinary(binaryId);

        BinaryContent binaryContent = binaryContentRepository.findByBinaryId(binaryId).orElse(null);

        if (null != binaryContent) {
            bytes = binaryContent.getValue();
        } else {
            bytes = new byte[0];
        }

    }

    private BinaryImpl findBinary(String binaryId) {
        binary = binaryRepository.findById(binaryId).orElse(null);

        if (null == binary) {
            throw new BinaryNotFoundException("Unable to locate a binary for binaryId '" + binaryId + "'");
        }
        return binary;
    }

    public byte[] getBytes() {
        return bytes;
    }

    public BinaryImpl getBinary() {
        return binary;
    }

    public void setBinaryId(String binaryId) {
        this.binaryId = binaryId;
    }
}
