package com.umss.tis.binaries.service.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Santiago Mamani
 */

@Configuration
@EnableSwagger2
class SwaggerConfig {

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("binaries-api")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.umss.tis.binaries.service.controller.api"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }

    @Bean
    public Docket system() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("binaries-system")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.umss.tis.binaries.service.controller.system"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(apiInfo());
    }


    private ApiInfo apiInfo() {

        return new ApiInfoBuilder()
                .title("Binaries Service API")
                .description("Binaries management")
                .contact(new Contact("santiago", "", "sm.zeballos.umss@gmail.com"))
                .version("0.0.1")
                .license("Apache 1.0")
                .licenseUrl("https://docs.spring.io/spring-data/jpa/docs/current/reference/html/")
                .build();
    }
}