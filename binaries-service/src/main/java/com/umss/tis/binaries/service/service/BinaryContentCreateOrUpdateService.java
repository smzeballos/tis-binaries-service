package com.umss.tis.binaries.service.service;

import com.umss.tis.binaries.service.model.domain.BinaryContent;
import com.umss.tis.binaries.service.model.domain.BinaryImpl;
import com.umss.tis.binaries.service.model.repository.BinaryContentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Santiago Mamani
 */
@Service
public class BinaryContentCreateOrUpdateService {

    private BinaryImpl binary;

    private byte[] bytes;

    @Autowired
    private BinaryContentRepository repository;

    public void execute() {
        BinaryContent binaryContent = findBinaryContent(binary.getId());

        if (null == binaryContent) {
            binaryContent = new BinaryContent();
            binaryContent.setBinary(binary);
            binaryContent.setBinaryId(binary.getId());
        }

        binaryContent.setValue(bytes);

        repository.save(binaryContent);
    }

    private BinaryContent findBinaryContent(String binaryId) {
        return repository.findByBinaryId(binaryId).orElse(null);
    }

    public void setBinary(BinaryImpl binary) {
        this.binary = binary;
    }

    public void setBytes(byte[] bytes) {
        this.bytes = bytes;
    }
}
