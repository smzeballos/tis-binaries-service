package com.umss.tis.binaries.service.model.domain;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.DBRef;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author Santiago Mamani
 */
@Document(collection = "BinaryContent")
public class BinaryContent {

    @Id
    private String id;

    @Field("value")
    private byte[] value;

    @Field("binaryId")
    private String binaryId;

    @DBRef
    private BinaryImpl binary;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public byte[] getValue() {
        return value;
    }

    public void setValue(byte[] value) {
        this.value = value;
    }

    public String getBinaryId() {
        return binaryId;
    }

    public void setBinaryId(String binaryId) {
        this.binaryId = binaryId;
    }

    public BinaryImpl getBinary() {
        return binary;
    }

    public void setBinary(BinaryImpl binary) {
        this.binary = binary;
    }
}
