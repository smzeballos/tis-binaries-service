package com.umss.tis.binaries.service.service;

import com.umss.tis.binaries.service.exception.BinaryCanNotReadException;
import com.umss.tis.binaries.service.model.domain.BinaryImpl;
import com.umss.tis.binaries.service.model.repository.BinaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;

/**
 * @author Santiago Mamani
 */
abstract class AbstractBinaryService {

    private MultipartFile file;

    private BinaryImpl binary;

    @Autowired
    private BinaryRepository repository;

    @Autowired
    private BinaryContentCreateOrUpdateService binaryContentCreateOrUpdateService;

    public void execute() {
        BinaryImpl instance = loadBinaryInstance();

        putFileInformation(instance);

        binary = repository.save(instance);

        saveFileContent();
    }

    protected abstract BinaryImpl loadBinaryInstance();

    private void putFileInformation(BinaryImpl instance) {
        instance.setFileName(file.getOriginalFilename());
        instance.setFileSize(file.getSize());
        instance.setMimeType(file.getContentType());
    }

    private void saveFileContent() {
        try {
            binaryContentCreateOrUpdateService.setBinary(binary);
            binaryContentCreateOrUpdateService.setBytes(file.getBytes());

            binaryContentCreateOrUpdateService.execute();
        } catch (IOException e) {
            throw new BinaryCanNotReadException("Cannot read file");
        }
    }

    public void setFile(MultipartFile file) {
        this.file = file;
    }

    public BinaryImpl getBinary() {
        return binary;
    }

}
