package com.umss.tis.binaries.service.model;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * @author Santiago Mamani
 */
@Configuration
@EnableMongoRepositories(
        basePackages = "com.umss.tis.binaries.service.model.repository"
)
public class ModelConfig {
}
