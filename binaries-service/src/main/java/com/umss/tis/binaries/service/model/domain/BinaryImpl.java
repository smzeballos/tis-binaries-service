package com.umss.tis.binaries.service.model.domain;

import com.umss.tis.binaries.api.model.Binary;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

/**
 * @author Santiago Mamani
 */
@Document(collection = "binary")
public class BinaryImpl implements Binary {

    @Id
    private String id;

    @Field("mimetype")
    private String mimeType;

    @Field("filename")
    private String fileName;

    private Long fileSize;

    @Override
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String getMimeType() {
        return mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    @Override
    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    @Override
    public Long getFileSize() {
        return fileSize;
    }

    public void setFileSize(Long fileSize) {
        this.fileSize = fileSize;
    }
}
