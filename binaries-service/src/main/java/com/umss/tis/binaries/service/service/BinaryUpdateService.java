package com.umss.tis.binaries.service.service;

import com.umss.tis.binaries.service.exception.BinaryNotFoundException;
import com.umss.tis.binaries.service.model.domain.BinaryImpl;
import com.umss.tis.binaries.service.model.repository.BinaryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author Santiago Mamani
 */
@Service
public class BinaryUpdateService extends AbstractBinaryService {

    private String binaryId;

    @Autowired
    private BinaryRepository repository;

    @Override
    protected BinaryImpl loadBinaryInstance() {
        return repository.findById(binaryId)
                .orElseThrow(() -> new BinaryNotFoundException("Unable to locate a binary for binaryId '" + binaryId + "'"));
    }

    public void setBinaryId(String binaryId) {
        this.binaryId = binaryId;
    }
}
