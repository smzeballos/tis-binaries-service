package com.umss.tis.binaries.service.model.repository;

import com.umss.tis.binaries.service.model.domain.BinaryContent;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.util.Optional;

/**
 * @author Santiago Mamani
 */
public interface BinaryContentRepository extends MongoRepository<BinaryContent, String> {

    Optional<BinaryContent> findByBinaryId(String binaryId);
}
