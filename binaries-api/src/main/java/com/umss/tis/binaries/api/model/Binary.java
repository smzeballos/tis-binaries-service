package com.umss.tis.binaries.api.model;

/**
 * @author Santiago Mamani
 */
public interface Binary {

    String getId();

    String getMimeType();

    String getFileName();

    Long getFileSize();
}
